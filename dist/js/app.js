'use strict';

$(document).ready(function () {

  $(".tab-container .tabs ul li").on("click", function () {
    var id = $(this).attr("data-tab");
    var $this = $(this).closest('.tab-container');
    console.log($this);
    $this.find("li").removeClass("is-active");
    $this.find(".tab-content").removeClass("current-tab");
    $(this).addClass("is-active");
    $("#" + id).addClass("current-tab");
  });


  $("#cookie").on("click", function() {
    $(this).closest(".notification").hide("fast");
  })

  $(".logoslider").slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    focusOnSelect: true,
    dots: false,
    arrows: false,
    initialSlide: 5
  });

  $(".testimonials").slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    arrows: true
  });



  $(".oplossingenslider").slick({
    speed: 300,
    arrows: false,
    dots: false,
    vertical: true,
    autoplay: true,
    verticalSwiping: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });
  

  
  
  $('.logoslider img').plate();

  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  });

  // reveal navbar when scroll up
  var prev = 0;
  var $window = $(window);
  var nav = $('.navbar');
  
  $window.on('scroll', function(){
    var scrollTop = $window.scrollTop();

    if (scrollTop < 100) {
      nav.removeClass('hidden');
      nav.removeClass('scroll');
    } else {  
      nav.toggleClass('hidden', scrollTop > prev);
      nav.toggleClass('scroll', scrollTop > 100 );
      prev = scrollTop;
    }

  });


});


$(window).on('scroll', function() {
  $(".viewport").each(function() {
    if (isScrolledIntoView($(this))) {
      $(this).addClass("is-visible");
    }
  });
});

function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();
  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$(window).on('load', function() {
  $("html").addClass("loaded");
})

