var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var gulpkss = require('gulp-kss');
var gulpclean = require('gulp-clean');
var gulpconcat = require('gulp-concat');
var gulpclean = require('gulp-clean');
var autoprefixer = require('gulp-autoprefixer');
var nunjucksRender = require('gulp-nunjucks-render');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function () {
    return gulp.src('src/sass/*.sass')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

// Move the javascript files into our /src/js folder
gulp.task('js', function () {
    return gulp.src(['src/js/*.js'])
        .pipe(gulp.dest("dist/js"))
        .pipe(browserSync.stream());
});

gulp.task('copy', function () {
    return gulp.src(['src/assets/*'])
        .pipe(gulp.dest('dist/assets/'))
        .pipe(gulp.dest('docs/styleguide/assets/'));
});

// gulp.task('kss-clean', function () {
//     return gulp.src('./dist/docs')
//         .pipe(gulpclean());
// });

// gulp.task('kss', ['sass', 'kss-clean'], function () {
//     // generate docs
//     gulp.src(['src/sass/**/*.sass'])
//         .pipe(gulpkss({
//             overview: 'src/docs/index.md',
//             templateDirectory: '/ibstyleguide'
//         }))
//         .pipe(gulp.dest('dist/docs'));

//     // copy styles.css for docs
//     gulp.src('dist/css/app.css')
//         .pipe(gulpconcat('/dist/docs/public/style.css'))
//         .pipe(gulp.dest('dist/docs/'));
// });

gulp.task('build', ['assets']);

//gulp.task('build', ['copy']);

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function () {

    browserSync.init({
        server: "./dist/"
    });

    gulp.watch(['src/**/*.sass'], ['sass']);
    gulp.watch(['src/assets/*'], ['copy']);
    gulp.watch(['src/templates/**/*.+(nj)', 'src/pages/**/*.+(nj)'], ['nj']).on('change', browserSync.reload);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', ['js', 'serve', 'nj']);


gulp.task('nj', function () {
    return gulp.src('src/pages/*.nj')
    .pipe(nunjucksRender({
        path: ['src/templates/']
    }))
    .pipe(gulp.dest('dist'));
});

