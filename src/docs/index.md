Ibuildings Styleguide
=================

This is an ui-library for the Ibuildings website.

# Ibuildings frontend

* sass - css preprocessor
* browsersync - live preview + webserver
* KSS - Living styleguide
* Bulma - css framework
* Nunjucks - templating systeme

--------

# Download and run

```
git clone git@bitbucket.org:pixelkings/ibuildings2019.git
```
---

```
yarn install

```
---

```
gulp
```

---


# Nunjucks

Nunjucks is a javascript templating system
* src/
* src/pages
* src/templates
* src/templates/partials



# KSS

KSS is a living styleguide system. 


Generate a new styleguide:
```
yarn run gulp
```
